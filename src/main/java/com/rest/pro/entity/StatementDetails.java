package com.rest.pro.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

//"transaction_id": 1,
//"amount": 100.00,
//"type": "WITHDRAWAL",
//"old_balance": 200.00,
//"new_balance" : 100.00,
//"transaction_date": "2021-07-14T04:47:15+0000",
//"transaction_status": "SUCCESS",
//"created_at": "2021-07-14T04:47:15+0000",
//"updated_at": "2021-07-14T04:47:15+0000" 

public class StatementDetails {
	
	
	private Long AccountNumber;
	private String AccountHolderName;
	private LocalDate DateOfBirth;
	private String AccountType;
	private Double Balance;
	private List<com.rest.pro.entity.Transaction> transactions = new ArrayList<>();
	private LocalDate FromDate;
	private LocalDate ToDate;
	
	
	
	public Long getAccountNumber() {
		return AccountNumber;
	}
	public void setAccountNumber(Long accountNumber) {
		AccountNumber = accountNumber;
	}
	public String getAccountHolderName() {
		return AccountHolderName;
	}
	public void setAccountHolderName(String accountHolderName) {
		AccountHolderName = accountHolderName;
	}
	public LocalDate getDateOfBirth() {
		return DateOfBirth;
	}
	public void setDateOfBirth(LocalDate dateOfBirth) {
		DateOfBirth = dateOfBirth;
	}
	public String getAccountType() {
		return AccountType;
	}
	public void setAccountType(String accountType) {
		AccountType = accountType;
	}
	public Double getBalance() {
		return Balance;
	}
	public void setBalance(Double balance) {
		Balance = balance;
	}
	public LocalDate getFromDate() {
		return FromDate;
	}
	public void setFromDate(LocalDate fromDate) {
		FromDate = fromDate;
	}
	public LocalDate getToDate() {
		return ToDate;
	}
	public void setToDate(LocalDate toDate) {
		ToDate = toDate;
	}
	public List<Transaction> getTransactions() {
		return transactions;
	}
	public void setTransactions(List<com.rest.pro.entity.Transaction> transactions) {
		this.transactions = transactions;
	}
	
	

}
