package com.rest.pro.repo;



import com.rest.pro.entity.BankAccount;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "api",path = "api")
public interface BankRepository  extends JpaRepository<BankAccount,Long>
{
	   @Query("from BankAccount where account_number=?1")
	   BankAccount findByAccountNumber(Long accountNumber);
	   
//	   @Query("from BankAccount select balance where account_number=?1")
//	   BankAccount findByBalance(Long accountNumber);
//	
}
