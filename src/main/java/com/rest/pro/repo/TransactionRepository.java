package com.rest.pro.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.rest.pro.entity.Transaction;


@RepositoryRestResource(collectionResourceRel = "apit",path = "apit")
public interface TransactionRepository extends JpaRepository<Transaction, Long>
{



}
