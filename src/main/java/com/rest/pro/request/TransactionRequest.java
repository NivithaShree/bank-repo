package com.rest.pro.request;

public class TransactionRequest {
	
//	"accountHolderName":"Nivitha1",
//    "dateofBirth":"2021-07-17",
//    "accountType":"Savings",
//    "initialDeposit":300.00
		
	    private double TransactionAmount;
		private String TransactionType;
		private String TransactionStatus;
		public double getTransactionAmount() {
			return TransactionAmount;
		}
		public void setTransactionAmount(double transactionAmount) {
			TransactionAmount = transactionAmount;
		}
		public String getTransactionType() {
			return TransactionType;
		}
		public void setTransactionType(String transactionType) {
			TransactionType = transactionType;
		}
		public String getTransactionStatus() {
			return TransactionStatus;
		}
		public void setTransactionStatus(String transactionStatus) {
			TransactionStatus = transactionStatus;
		}
		
		
		
		
}