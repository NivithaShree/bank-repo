package com.rest.pro.controller;


import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.rest.pro.entity.BankAccount;
import com.rest.pro.entity.Transaction;
import com.rest.pro.repo.BankRepository;
import com.rest.pro.repo.TransactionRepository;
import com.rest.pro.request.BankRequest;
import com.rest.pro.response.BadRequestException;
import com.rest.pro.validate.Validation;
import com.rest.pro.response.Error;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
@RestController
public class BankController {
	
    @Autowired
    BankRepository dao;
    
    @Autowired
    TransactionRepository da;
    
    @Autowired
    private Validation validator;
    
    static final Logger logger = LogManager.getLogger(BankController.class.getName());

    @PostMapping(path="bank/create",consumes={"application/json"})
    public BankAccount home(@RequestBody BankRequest obj)
    {
        System.out.println(obj.getAccountType()+obj.getAccountHolderName()+obj.getDateofBirth()+obj.getInitialDeposit());
        BankAccount objAcc = new BankAccount();
        logger.info("Post Mapping of Create Request Begins");
        objAcc.setAccountHolderName(obj.getAccountHolderName());
        objAcc.setAccountType(obj.getAccountType());
        objAcc.setDateofBirth(obj.getDateofBirth());
        
        if ((obj.getInitialDeposit() != 0.0)){
             objAcc.setBalance(obj.getInitialDeposit());
        }
        else
        {
        	objAcc.setBalance(0.0);
        }
        if ((obj.getAccountType().toLowerCase()).equals("current")){
            objAcc.setTransactionFee(5.0);
        }
        
        List<Error> errors = validator.validateCreateRequest(objAcc);
        
        // if not success
        if(errors.size()>0) {
        	throw new BadRequestException("Bad Request",errors);
        }
        
        // if success
        dao.save(objAcc);
        
        
        Transaction obj1 = new Transaction();
        
        obj1.setTransactionAmount(objAcc.getBalance());
        obj1.setTransactionType("deposit");
        obj1.setTransactionStatus("Success");
        obj1.setAccount(objAcc);
        obj1.setOldBalance(0.0);
        obj1.setNewBalance(objAcc.getBalance());
        if(obj1.getNewBalance()!=null && obj1.getNewBalance()!=0.0)
            objAcc.getTransaction().add(obj1);
        
        da.save(obj1);
        
        
        return objAcc;
    }
    

}