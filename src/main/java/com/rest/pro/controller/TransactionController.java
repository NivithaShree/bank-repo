package com.rest.pro.controller;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.rest.pro.response.BadRequestException;
import com.rest.pro.response.Error;
import com.rest.pro.repo.BankRepository;
import com.rest.pro.repo.TransactionRepository;
import com.rest.pro.entity.BankAccount;
import com.rest.pro.entity.Transaction;
import com.rest.pro.request.TransactionRequest;
import com.rest.pro.validate.TransactionValidate;
import com.rest.pro.validate.Validation;
@RestController
public class TransactionController {
	 @Autowired
	 TransactionRepository da;
	 
	 @Autowired
	 BankRepository dao;
	 
	 @Autowired
	 private Validation validator;
	 
	 @Autowired
	 private TransactionValidate validate;
	 
	 static final Logger logger = LogManager.getLogger(BankController.class.getName());
	 
	 @PostMapping(path="bank/transaction/{acc}",consumes={"application/json"})
	 public Transaction ho(@PathVariable("acc") Long AccountNumber, @RequestBody TransactionRequest obj){
	    	
	        Transaction objAcc = new Transaction();
	        logger.info("Post Mapping of Transaction Request Begins");
	        
	        List<Error> errors = validate.validateTransactionRequest(AccountNumber, obj);
			
		    if(errors.size()>0) 
		    {
		    	throw new BadRequestException("Bad Request",errors);
		    }
	        
	        
	        BankAccount ob = dao.findByAccountNumber(AccountNumber);
	        
	        BankAccount ob1 = dao.getById(AccountNumber);
	       
		        
	        
	        objAcc.setTransactionAmount(obj.getTransactionAmount());
	        objAcc.setTransactionType(obj.getTransactionType());      
	        
	        objAcc.setOldBalance(ob1.getBalance());
	        
	        Double newbal = ob1.getBalance();
	        if(obj.getTransactionType().toLowerCase().equals("deposit"))
	        {
	        	newbal = newbal + (obj.getTransactionAmount() - ob1.getTransactionFee()) ;
	        	objAcc.setNewBalance(newbal);
	        	objAcc.setTransactionStatus("Success");
	        }
	        else if(obj.getTransactionType().toLowerCase().equals("withdrawal"))
	        {
	        	if(newbal > obj.getTransactionAmount())
	        	{
	        		newbal = newbal - (obj.getTransactionAmount() - ob1.getTransactionFee());
	        		objAcc.setNewBalance(newbal);
	        		objAcc.setTransactionStatus("Success");
	        	}
	        	else
	        	{
	        		objAcc.setNewBalance(ob1.getBalance());
	        		objAcc.setTransactionStatus("Failure");
	        	}	       	 	
	        }
	        ob.setBalance(newbal);
	        ob.getTransaction().add(objAcc);
	        objAcc.setAccount(ob);
	        da.save(objAcc);
	        
	        List<Error> errorsbal = validate.validateBalanceRequest(AccountNumber, obj);
	        
	        // if not success
	        if(errorsbal.size()>0) {
	        	throw new BadRequestException("Bad Request",errorsbal);
	        }
	        
	  
	        return objAcc;
	          
	    }  
}