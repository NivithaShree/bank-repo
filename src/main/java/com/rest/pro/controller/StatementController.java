package com.rest.pro.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rest.pro.entity.BankAccount;
import com.rest.pro.entity.StatementDetails;
import com.rest.pro.entity.Transaction;
import com.rest.pro.repo.BankRepository;
import com.rest.pro.repo.TransactionRepository;
import com.rest.pro.request.StatementRequest;
import com.rest.pro.request.TransactionRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
@RestController
public class StatementController {
	
	@Autowired
	BankRepository dao;
	
	@Autowired
	TransactionRepository da;
	
	static final Logger logger = LogManager.getLogger(BankController.class.getName());
	
	@GetMapping(path="bank/details/{acc}")
    public StatementDetails details(@PathVariable("acc") Long AccountNumber, @RequestParam("fromDate") String fromDate1, @RequestParam("toDate") String toDate1){
    	
        Transaction objAcc = new Transaction();
        
        logger.info("Get Mapping of Transaction List Request Begins");
        BankAccount ob = dao.findByAccountNumber(AccountNumber);
        
        BankAccount ob1 = dao.getById(AccountNumber);
        
        StatementDetails sd = new StatementDetails();
        
        List<com.rest.pro.entity.Transaction> list1 = ob.getTransaction();
        List<com.rest.pro.entity.Transaction> list2 = new ArrayList<>();
        
        com.rest.pro.entity.Transaction trans = null;
        
        LocalDate fromDate = LocalDate.parse(fromDate1);
        LocalDate toDate = LocalDate.parse(toDate1);
        
        for(int i=0;i<list1.size();i++)
        {
        	LocalDate date1 = list1.get(i).getTransactionDate();
        	LocalDate fromdate = fromDate;
        	LocalDate todate = toDate;
        	
        	if((fromdate.compareTo(date1) == -1) && (todate.compareTo(date1) == 1))
        	{
        		trans = list1.get(i);
        	}
        	if(trans!=null)
        	{
        		list2.add(trans);
        	}     	
        	
        }
        
        sd.setAccountNumber(AccountNumber);
        sd.setAccountHolderName(ob.getAccountHolderName());
        sd.setDateOfBirth(ob.getDateofBirth());
        sd.setBalance(ob.getBalance());
        sd.setAccountType(ob.getAccountType());
        sd.setFromDate(fromDate);
        sd.setToDate(toDate);
        sd.setTransactions(list2);
//        System.out.println(list2);
//        System.out.println(list1);
        
        
       return sd;
          
    }  
}
