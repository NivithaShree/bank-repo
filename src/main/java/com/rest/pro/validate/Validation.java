package com.rest.pro.validate;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rest.pro.entity.BankAccount;
import com.rest.pro.entity.Transaction;
import com.rest.pro.repo.BankRepository;
import com.rest.pro.request.TransactionRequest;
import com.rest.pro.response.Error;

@Component
public class Validation {

	
	@Autowired
	BankRepository repo;
	public List<Error> validateCreateRequest(BankAccount objAcc) {
		
		List<Error> errors = new ArrayList<>();
		
		
		if(objAcc.getAccountHolderName()==null) {
			Error error = new Error("name","AccountHolderName is null");
			errors.add(error);
			
		}
		
		if(objAcc.getAccountType()==null) {
			Error error = new Error("AccountType","AccountType is null");
			errors.add(error);
		}
		else if(objAcc.getAccountType().toLowerCase().equals("savings")==false && objAcc.getAccountType().toLowerCase().equals("current")==false) {
			Error error = new Error("AccountType","AccountType is not valid");
			errors.add(error);
		}
		if(objAcc.getDateofBirth()==null) {
			Error error = new Error("DOB","DataOfBirth is null");
			errors.add(error);
			
		}

		return errors;
	}

}
