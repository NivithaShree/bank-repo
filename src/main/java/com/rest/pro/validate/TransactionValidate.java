package com.rest.pro.validate;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import com.rest.pro.entity.BankAccount;
import com.rest.pro.repo.BankRepository;
import com.rest.pro.request.TransactionRequest;
import com.rest.pro.response.Error;

@Component
public class TransactionValidate {
	
	@Autowired
	BankRepository repo;
	
	
	public List<Error> validateTransactionRequest(Long AccountNumber, TransactionRequest obj) {
		
		List<Error> errors = new ArrayList<>();
		BankAccount ob = repo.findByAccountNumber(AccountNumber);
		
		if(ob == null)
		{
			Error error = new Error("AccountNumber","Invalid Account Number");
			errors.add(error);
		}
		
		
		if(obj.getTransactionType() == null)
		{
			Error error = new Error("TransactionType","Enter Transaction Type");
			errors.add(error);
		}
		
		if(obj.getTransactionAmount() <= 0.0) 
		{
			Error error = new Error("TransactionAmount","Invalid Trasaction Amount");
			errors.add(error);
			
		}
		if(obj.getTransactionType()!=null)
		{
			if((obj.getTransactionType().toLowerCase()).equals("withdrawal") == false && (obj.getTransactionType().toLowerCase()).equals("deposit") == false) 
			{
				Error error = new Error("TransactionType","Enter Valid Transaction Type");
				errors.add(error);
				
			}
		}
		return errors;
	}
	public List<Error> validateBalanceRequest(Long AccountNumber, TransactionRequest obj) {
		
		List<Error> errorsbal = new ArrayList<>();
		BankAccount ob = repo.findByAccountNumber(AccountNumber);
	
		Double newbal = obj.getTransactionAmount();
		Double oldbal = ob.getBalance();
		
		if(newbal > oldbal)
		{
			Error error = new Error("TransactionAmount","Invalid Balance");
			errorsbal.add(error);
		}
		
		return errorsbal;
	}

}
